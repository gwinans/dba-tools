#!/usr/bin/env bash

# Set tool_name for logging/alerts
tool_name='HEARTBEAT_DAEMON'

# Set base_dir and go to it.
base_dir=$( dirname "$( readlink -f $0 )" )
cd "${base_dir}"

# Start / Ensure pt-heartbeat is running

## Import required info / config / libraries
## Local includes
source config/base_config.sh

# Util library includes.
source lib/logger.sh

running=$( ps -ef | grep pt-heartbeat | grep -v grep | grep pt-heartbeat-daemon )

if [[ -z "${running}" ]]; then
    logger "Heartbeat not running. Starting ... "
    pt-heartbeat --config=config/pt-heartbeat-daemon.conf
else
    logger "Heartbeat is running."
fi
