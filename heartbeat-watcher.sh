#!/usr/bin/env bash

# Set tool_name for logging/alerts
tool_name='HEARTBEAT_WATCHER'

# Set base_dir and go to it.
base_dir=$( dirname "$( readlink -f $0 )" )
cd "${base_dir}"

# This script is used to parse output from pt-heartbeat running on a slave.

## Import required info / config / libraries

## Local includes
source config/base_config.sh

# Util library includes.
source lib/logger.sh

db_opts=( "-u${db_user}" )

if [[ -n "${db_pass}" ]]; then
    db_opts+=( "-p${db_pass}" )
fi

logger "Beginning check ... "

dbdist=$( mysql --version | grep MariaDB )

if [[ -n "${dbdist}" ]]; then
    show_slave="SHOW ALL SLAVES STATUS\G"
else
    show_slave="SHOW SLAVE STATUS\G"
fi

# Get the Master_Server_Id(s) to loop through
master_id=( $( mysql "${db_opts[@]}" -e "${show_slave}\G" 2>&1 | grep -v "Using a password" | grep Master_Server_Id | awk '{print $2}' ) )

# Step through the found Id(s) and alert if any are > warn/crit seconds
for id in "${master_id[@]}"
do
    res=$( pt-heartbeat --config=config/pt-heartbeat-watcher.conf --master-server-id ${id} | cut -d . -f 1 )
    if [[ "${res}" -gt "${warning_lag_seconds}" ]]; then
        if [[ "${res}" -gt "${critical_lag_seconds}" ]]; then
            logger "Replication state is critical for Master_Server_Id: ${id}. Alerting ..."
            exit 0
        fi
        logger "Replication is in warning state for Master_Server_Id: ${id}. Alerting ..."
        exit 0
    fi
    logger "Replication is healthy for Master_Server_Id: ${id}!"
done
