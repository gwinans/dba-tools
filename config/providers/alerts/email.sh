## Enables the email alert provider
## Set to 0 to turn it off
email=1

# This is a bash array.
# Example: EMAIL_LIST=( "me@place.com" "you@otherplace.com" )
email_list=()
