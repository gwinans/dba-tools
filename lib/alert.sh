#!/usr/bin/env bash
shopt -s nullglob

source config/base_config.sh

# Runs providers to handle alerting

# Alerting enabled? If not, simply do nothing.
if [[ ${alerting} -eq 1 ]]; then

    # Cleanup alert locks > alert_sleep_minutes
    find /tmp -name '*.alert.lock' -mmin $((${alert_sleep_minutes} - 1)) -delete

    # Alerting is enabled, is stop_alert_spam on AND an alert lock file exists?
    if [[ ${stop_alert_spam} -eq 1 ]] && [[ -f "/tmp/${tool_name}.alert.lock" ]]; then
        logger "An alert was sent in the last ${alert_sleep_minutes} minute(s) ..."
        logger "You can manually clear this with the following command:"
        logger "  rm -f /tmp/${tool_name}.alert.lock"
    else
        # Touch an alert lock file whether stop_alert_spam is on or not.
        touch "/tmp/${tool_name}.alert.lock"

        # Nothing to stop us from alerting now!
        # Step through all scripts found in lib/providers/alerts
        providers=( "${base_dir}"/lib/providers/alerts/*.sh )
        for provider in "${providers[@]}"
        do
            source "${provider}"
        done
    fi
fi
