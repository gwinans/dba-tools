logger() {

    local message="${1}"
    local log_date_format=$( date "+%F %H:%M:%S")

    if [[ ! -e "${log_dir}" ]]; then
        mkdir -p "${log_dir}"
    fi

    if [[ "${verbose}" -eq 1 ]]; then
        echo "${message}"
    fi

    if [[ -z "${log_file}" ]]; then
        echo "Log file not specified. Defaulting to /tmp/${tool_name}.log"
        log_file="/tmp/${tool_name}.log"
    fi

    if [[ -w "${log_dir}" ]]; then
        echo "[${log_date_format}] ${message}" >> ${log_file}
    fi

}
