#!/usr/bin/env bash

source config/providers/alerts/slack.sh

if [[ ${slack} -eq 1 ]]; then
    if [[ -n "${slack_webhook_url}" ]]; then
        json="{\"channel\": \"${slack_channel}\", \"username\":\"${slack_username}\", \"icon_emoji\":\"fire\", \"attachments\":[{\"color\":\"danger\" , \"text\":\"${tool_name} failed on ${hostname_short}\"}]}"
        curl -s -d "payload=${json}" "${slack_webhook_url}"
    else
        logger "Slack alert is enabled, but the slack_webhook_url is not set in ${base_dir}/config/providers/alerts/slack.sh"
    fi
fi
