#!/usr/bin/env bash

# Set tool_name for logging/alerts
tool_name='MAX_CONNECTION_WATCHER'

# Set base_dir and go to it.
base_dir=$( dirname "$( readlink -f $0 )" )
cd "${base_dir}"

## Import required config / libraries
## Local includes
source config/base_config.sh

# Util library includes.
source lib/logger.sh

db_opts=( "-u${db_user}" )

if [[ -n "${db_pass}" ]]; then
    db_opts+=( "-p${db_pass}" )
fi

max_conn=$( mysql -sN "${db_opts[@]}" -e "SHOW VARIABLES LIKE 'max_connections'" 2>&1 | grep -v "Using a password" | awk '{print $2}' )
threads_connected=$( mysql -sN "${db_opts[@]}" -e "SHOW STATUS LIKE 'threads_connected'" 2>&1 | grep -v "Using a password" | awk '{print $2}' )

desired_max=$( echo "${max_conn} * ${max_conn_allowed_pct}" | bc | awk '{print int($1+0.5)}' )

if (( ${res} ->= ${desired_max} )); then
    logger "max_connections > ${desired_max}"
fi
